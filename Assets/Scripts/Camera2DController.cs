﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera2DController : MonoBehaviour {

    public float distance = 5f;
    public int orthographicSizeMin = 1;
    public int orthographicSizeMax = 6;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.D))
        {
            //transform.position = new Vector3(transform.position.x + distance, transform.position.y, transform.position.z);
            transform.position = transform.position + Camera.main.transform.right * distance * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.A))
        {
            //transform.position = new Vector3(transform.position.x - distance, transform.position.y, transform.position.z);
            transform.position = transform.position - Camera.main.transform.right * distance * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.position = transform.position - Camera.main.transform.up * distance * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.W))
        {
            transform.position = transform.position + Camera.main.transform.up * distance * Time.deltaTime;
        }

        if (Input.GetAxis("Mouse ScrollWheel") > 0) // forward
        {
            if (transform.position.y - Camera.main.transform.forward.y > 8)
                transform.position = transform.position + Camera.main.transform.forward * 4*distance * Time.deltaTime;
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0) // back
        {
            transform.position = transform.position - Camera.main.transform.forward * 4*distance * Time.deltaTime;
        }
        
    }
}
