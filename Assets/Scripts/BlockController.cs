﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BlockController : MonoBehaviour {

	private bool isSelected = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnMouseOver(){
		if((GameController.instance != null && (GameController.instance.mode ==2 || GameController.instance.mode==1)) || MapEditorController.instance != null)
			setHighlight (true);
	}

	private void OnMouseDown(){
		if (GameController.instance != null && GameController.instance.mode == 1) { // despawn mode
			if (GameController.instance.selectedBlock != null) {
				GameController.instance.selectedBlock.GetComponent<BlockController> ().setSelected (false);
				GameController.instance.selectedBlock = gameObject;
				int x = (int)(gameObject.transform.position.x);
				int z = (int)(gameObject.transform.position.z);
				gameObject.tag = "DESPAWN";
				setSelected (true);
				Debug.Log(string.Format("[{0}][{1}] is now despawn point",x,z));
			}
			GameController.instance.selectedBlock = gameObject;
			int x1 = (int)(gameObject.transform.position.x);
			int z1 = (int)(gameObject.transform.position.z);
			gameObject.tag = "DESPAWN";
			Debug.Log(string.Format("[{0}][{1}] is now despawn point with tag",x1,z1, gameObject.tag));
			setSelected (true);
		}
		else if (GameController.instance != null && GameController.instance.mode == 2) { // spawn mode
			if (GameController.instance.selectedBlock != null) {
				GameController.instance.selectedBlock.GetComponent<BlockController> ().setSelected (false);
				GameController.instance.selectedBlock = gameObject;
				gameObject.tag = "SPAWN";
				int x = (int)(gameObject.transform.position.x);
				int z = (int)(gameObject.transform.position.z);
				setSelected (true);
				Debug.Log(string.Format("[{0}][{1}] is now spawn point",x,z));
			}
			GameController.instance.selectedBlock = gameObject;
			int x1 = (int)(gameObject.transform.position.x);
			int z1 = (int)(gameObject.transform.position.z);
			gameObject.tag = "SPAWN";
			Debug.Log(string.Format("[{0}][{1}] is now spawn point with tag",x1,z1, gameObject.tag));
			setSelected (true);
		}
        else if(MapEditorController.instance != null){
            changeBlock();
        }
	}

    private void OnMouseEnter()
    {
        if (MapEditorController.instance != null && Input.GetMouseButton(0))
        {
            changeBlock();
        }
    }

    private void OnMouseExit(){
		if (!isSelected)
			setSelected (false);
	}

    private void changeBlock()
    {
        MapEditorController.instance.changeBlock(this.gameObject);
    }

	public void setSelected(bool selected){
		setHighlight (selected);
		isSelected = selected;
	}

	public void setHighlight(bool highlight){
		if(highlight)
			GetComponent<Renderer> ().material.shader = Shader.Find ("Self-Illumin/Diffuse");
		else
			GetComponent<Renderer> ().material.shader = Shader.Find ("Standard");
	}
}
