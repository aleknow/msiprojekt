﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapEditorController : MonoBehaviour {

    public static MapEditorController instance = null;
    public MapLoader mapLoader;

    public Transform[,] map;

    public Dropdown blocksListDropdown;
    public InputField mapNameField;

    private const string GRASS_BLOCK = "Grass block";
    private const string WATER_BLOCK = "Water block";
    private const string PAVEMENT_BLOCK = "Pavement block";

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        //DontDestroyOnLoad (gameObject);
    }

    // Use this for initialization
    void Start () {
        if (PlayerPrefs.GetInt("existingMap") == 0) {
            map = mapLoader.createEmptyMap(PlayerPrefs.GetInt("width"), PlayerPrefs.GetInt("height"));
        }
        else if (PlayerPrefs.GetInt("existingMap") == 1)
        {
            string selectedMap = PlayerPrefs.GetString("mapName");
            string mapPath = Application.dataPath + "/Maps/" + selectedMap;
            map = mapLoader.loadMap(mapPath);
        }
        loadBlocksList();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void loadBlocksList(){
        blocksListDropdown.options.Add(new Dropdown.OptionData() { text = GRASS_BLOCK });
        blocksListDropdown.options.Add(new Dropdown.OptionData() { text = WATER_BLOCK });
        blocksListDropdown.options.Add(new Dropdown.OptionData() { text = PAVEMENT_BLOCK });
        blocksListDropdown.value = 1;
        blocksListDropdown.value = 0;
    }

    public void changeBlock(GameObject block){
        int x = (int)(block.transform.position.x);
        int z = (int)(block.transform.position.z);
        Destroy(block);

        switch (blocksListDropdown.options[blocksListDropdown.value].text)
        {
            case GRASS_BLOCK:
                map[x, z] = Instantiate(mapLoader.grassBlock, new Vector3(x, 0, z), Quaternion.identity);
                break;
            case WATER_BLOCK:
                map[x, z] = Instantiate(mapLoader.waterBlock, new Vector3(x, 0, z), Quaternion.identity);
                break;
            case PAVEMENT_BLOCK:
                map[x, z] = Instantiate(mapLoader.pavementBlock, new Vector3(x, 0, z), Quaternion.identity);
                break;
        }
    }

    public void saveMap()
    {
        mapLoader.saveMap(map, Application.dataPath + "/Maps/" + mapNameField.text + ".xml");
    }
}
