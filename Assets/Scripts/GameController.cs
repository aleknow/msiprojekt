﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	public static GameController instance = null;
	public MapLoader mapLoader;
	public GameObject selectedBlock = null;
	//public GameObject agent;

	string currentAgentBlock = "SportyGirl";


	public Button spawnButton;
	public Button despawnButton ;
	public Button nomalModeButton ;
	public Button addAgentButton ;
	public Button selectAgentToGenerate;
	public Button selectRunnerToGenerate;
	public Slider agentGenerationRateSlider;

	public Transform[,] map;
	// Use this for initialization

	public int mode=0;

	System.Random randomGenerator = new System.Random ();

	void Awake(){
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);

		//DontDestroyOnLoad (gameObject);
	}

	void Start () {
        string selectedMap = PlayerPrefs.GetString("selectedMap");
		string mapPath = Application.dataPath + "/Maps/" + selectedMap;
        map = mapLoader.loadMap (mapPath);
		AstarPath.active.Scan ();

		//Podczepiłem te funkcje w edytorze dla każdego przycisku. Bo tutaj nie wiem dlaczego, ale po kliknięciu odpalały się dwukrotnie :(
		//spawnButton.onClick.AddListener (setToSpawnMode);
		//despawnButton.onClick.AddListener (setToDespawnMode);
		//nomalModeButton.onClick.AddListener (setToNormalMode);
		//addAgentButton.onClick.AddListener (addAgent);
	}
	
	// Update is called once per frame
	void Update () {
		if (mode == 1) {
			//	Debug.Log ("DESPAWN MODE");
			GameObject[] objs = GameObject.FindGameObjectsWithTag ("DESPAWN");
			GameObject[] objsToClear = GameObject.FindGameObjectsWithTag ("SPAWN");
	//		Debug.Log (objs.Length);
			//	Debug.Log ("SPAWN MODE");
			foreach (GameObject despawnPoint in objs) {
				despawnPoint.GetComponent<Renderer> ().material.shader = Shader.Find ("Self-Illumin/Diffuse");
			}
			foreach (GameObject spawnPoint in objsToClear) {
				spawnPoint.GetComponent<Renderer> ().material.shader = Shader.Find ("Standard");
			}
		} else if (mode == 2) {

			GameObject[] objs = GameObject.FindGameObjectsWithTag ("SPAWN");
			GameObject[] objsToClear = GameObject.FindGameObjectsWithTag ("DESPAWN");
	//		Debug.Log (objs.Length);
			//	Debug.Log ("SPAWN MODE");
			foreach (GameObject spawnPoint in objs) {
				spawnPoint.GetComponent<Renderer> ().material.shader = Shader.Find ("Self-Illumin/Diffuse");
			}
			foreach (GameObject despawnPoint in objsToClear) {
				despawnPoint.GetComponent<Renderer> ().material.shader = Shader.Find ("Standard");
			}
		} else {
			GameObject[] objs = GameObject.FindGameObjectsWithTag ("SPAWN");
			GameObject[] objsToClear = GameObject.FindGameObjectsWithTag ("DESPAWN");
			foreach (GameObject spawnPoint in objs) {
				spawnPoint.GetComponent<Renderer> ().material.shader = Shader.Find ("Standard");
			}
			foreach (GameObject despawnPoint in objsToClear) {
				despawnPoint.GetComponent<Renderer> ().material.shader = Shader.Find ("Standard");
			}
		}

		generateAgent ();
	}

	void generateAgent(){
		double randomDouble = randomGenerator.NextDouble();
		if (randomDouble < agentGenerationRateSlider.value * Time.deltaTime)
			addAgent ();
	}

	public void setToSpawnMode()
	{
		mode = 2;
	}

	public void setToDespawnMode()
	{
		mode = 1;
	}
	public void setToNormalMode()
	{
		mode = 0;
	}
	public void addAgent()
	{
		GameObject[] spawnObjs = GameObject.FindGameObjectsWithTag ("SPAWN");
		GameObject[] despawnObjs = GameObject.FindGameObjectsWithTag ("DESPAWN");

		if (spawnObjs.Length != 0 && despawnObjs.Length != 0) {
			int randomStartPoint = randomGenerator.Next (0, spawnObjs.Length);
			int randomEndPoint = randomGenerator.Next (0, despawnObjs.Length);
			GameObject source = spawnObjs [randomStartPoint];

			var agent = Instantiate (Resources.Load<Transform> (currentAgentBlock), new Vector3 (source.transform.position.x, 0.5f, source.transform.position.z), Quaternion.identity);
		}
	}

    public void quitToMenu()
    {
        SceneManager.LoadScene("MenuScene");
    }
	public void selectGentleMan()
	{
		currentAgentBlock = "BlueSuitFree01";
	}

	public void selectRunner()
	{
		currentAgentBlock = "SportyGirl";
	}
}
