﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class MapParser : MonoBehaviour
{
    public static MapParser _instance = null;
    public int[,] loadMapFromXML(string mapPath){
		Mapa mapObject = loadMapObject (mapPath);

		int mapHeight = Int32.Parse(mapObject.Wysokosc);
		int mapWidth = Int32.Parse(mapObject.Szerokosc);

		int[,] map = new int[mapHeight, mapWidth];

		foreach(Pole pole in mapObject.Pola.Pole){
			map [Int32.Parse(pole.X), Int32.Parse(pole.Y)] = Int32.Parse(pole.Typ);
		}

		return map;
	}

	public Mapa loadMapObject(string mapPath){
		XmlSerializer mapSerializer = new XmlSerializer(typeof(Mapa));

		StreamReader reader = new StreamReader (mapPath);
		Mapa map = (Mapa)mapSerializer.Deserialize(reader);
		return map;
	}

    public void saveMapToXML(int[,] map, string mapName)
    {
        List<Pole> blockList = new List<Pole>();
        for (int x = 0; x < map.GetLength(0); x++)
        {
            for (int z = 0; z < map.GetLength(1); z++)
            {
                blockList.Add(new Pole() {
                    X = x.ToString(),
                    Y = z.ToString(),
                    Typ = map[x,z].ToString()
                });
            }
        }

        Pola blocks = new Pola() {
            Pole = blockList };
        Mapa xmlMap = new Mapa() {
            Wysokosc = map.GetLength(0).ToString(),
            Szerokosc = map.GetLength(1).ToString(),
            Pola = blocks };

        XmlSerializer mapSerializer = new XmlSerializer(typeof(Mapa));
        using (TextWriter writer = new StreamWriter(mapName))
        {
            mapSerializer.Serialize(writer, xmlMap);
        }
    }
}

[XmlRoot(ElementName="pole")]
public class Pole {
	[XmlElement(ElementName="x")]
	public string X { get; set; }
	[XmlElement(ElementName="y")]
	public string Y { get; set; }
	[XmlElement(ElementName="typ")]
	public string Typ { get; set; }
}

[XmlRoot(ElementName="pola")]
public class Pola {
	[XmlElement(ElementName="pole")]
	public List<Pole> Pole { get; set; }
}

[XmlRoot(ElementName="mapa")]
public class Mapa {
	[XmlElement(ElementName="wysokosc")]
	public string Wysokosc { get; set; }
	[XmlElement(ElementName="szerokosc")]
	public string Szerokosc { get; set; }
	[XmlElement(ElementName="pola")]
	public Pola Pola { get; set; }
}
	