﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
	public float distance = 5f;

	Vector2 _mouseAbsolute;
    Vector2 _smoothMouse;
 
    public Vector2 clampInDegrees = new Vector2(360, 180);
    public bool lockCursor;
    public Vector2 sensitivity = new Vector2(2, 2);
    public Vector2 smoothing = new Vector2(3, 3);
    public Vector2 targetDirection;
    public Vector2 targetCharacterDirection;

	// Use this for initialization
	void Start () {
		targetDirection = transform.localRotation.eulerAngles;
	}

	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.D)){
			//transform.position = new Vector3(transform.position.x + distance, transform.position.y, transform.position.z);
			transform.position = transform.position + Camera.main.transform.right * distance * Time.deltaTime;
		}
		if(Input.GetKey(KeyCode.A)){
			//transform.position = new Vector3(transform.position.x - distance, transform.position.y, transform.position.z);
			transform.position = transform.position - Camera.main.transform.right * distance * Time.deltaTime;
		}
		if(Input.GetKey(KeyCode.S)){
            //transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - speed);
            if (transform.position.y - Camera.main.transform.forward.y > 1)
                transform.position = transform.position - Camera.main.transform.forward * distance * Time.deltaTime;
		}
		if(Input.GetKey(KeyCode.W)){

            //transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + speed);
            if (transform.position.y+ Camera.main.transform.forward.y > 1)
                transform.position = transform.position + Camera.main.transform.forward * distance * Time.deltaTime;
		}
        if (Input.GetMouseButton(1)){
			rotateCamera ();
		}
	}

	private void rotateCamera(){
		Screen.lockCursor = lockCursor;

		// Allow the script to clamp based on a desired target value.
		var targetOrientation = Quaternion.Euler(targetDirection);
		var targetCharacterOrientation = Quaternion.Euler(targetCharacterDirection);

		// Get raw mouse input for a cleaner reading on more sensitive mice.
		var mouseDelta = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

		// Scale input against the sensitivity setting and multiply that against the smoothing value.
		mouseDelta = Vector2.Scale(mouseDelta, new Vector2(sensitivity.x * smoothing.x, sensitivity.y * smoothing.y));

		// Interpolate mouse movement over time to apply smoothing delta.
		_smoothMouse.x = Mathf.Lerp(_smoothMouse.x, mouseDelta.x, 1f / smoothing.x);
		_smoothMouse.y = Mathf.Lerp(_smoothMouse.y, mouseDelta.y, 1f / smoothing.y);

		// Find the absolute mouse movement value from point zero.
		_mouseAbsolute += _smoothMouse;

		// Clamp and apply the local x value first, so as not to be affected by world transforms.
		if (clampInDegrees.x < 360)
			_mouseAbsolute.x = Mathf.Clamp(_mouseAbsolute.x, -clampInDegrees.x * 0.5f, clampInDegrees.x * 0.5f);

		var xRotation = Quaternion.AngleAxis(-_mouseAbsolute.y, targetOrientation * Vector3.right);
		transform.localRotation = xRotation;

		// Then clamp and apply the global y value.
		if (clampInDegrees.y < 360)
			_mouseAbsolute.y = Mathf.Clamp(_mouseAbsolute.y, -clampInDegrees.y * 0.5f, clampInDegrees.y * 0.5f);

		transform.localRotation *= targetOrientation;

		// If there's a character body that acts as a parent to the camera

		var yRotation = Quaternion.AngleAxis(_mouseAbsolute.x, transform.InverseTransformDirection(Vector3.up));
		transform.localRotation *= yRotation;
	}
}
