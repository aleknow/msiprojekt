﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Pathfinding;

public class AgentController : MonoBehaviour {
	GameObject destination = null;

	public Transform agent;

	// Use this for initialization
	void Start () {
		// TUTAJ LOSUJEMY MU JAKIES DESTIANATION
		GameObject[] spawnObjs = GameObject.FindGameObjectsWithTag ("SPAWN");
		GameObject[] despawnObjs = GameObject.FindGameObjectsWithTag ("DESPAWN");
		if (spawnObjs.Length != 0 && despawnObjs.Length != 0) {
			System.Random rnd = new System.Random ();
			int randomEndPoint = rnd.Next (0, despawnObjs.Length);
			destination = despawnObjs [randomEndPoint];

			Seeker seeker = GetComponent<Seeker> ();
			seeker.StartPath (gameObject.transform.position, destination.transform.position, OnPathComplete);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Vector3.Distance(gameObject.transform.position, destination.transform.position) < 0.6)
			Destroy (gameObject);
	}

	public void OnPathComplete(Path p){
		if(p.error)
			Destroy (gameObject);
	}
}
