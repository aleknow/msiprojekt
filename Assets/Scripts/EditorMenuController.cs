﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EditorMenuController : MonoBehaviour {

    public Dropdown mapSelectDropdown;
    public InputField widthField;
    public InputField heightField;

    private string mapsDir;

    private void Awake()
    {
        mapsDir = Application.dataPath + "/Maps";
        loadMapsList();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void loadMapsList()
    {
        foreach (string mapFile in Directory.GetFiles(mapsDir, "*.xml", SearchOption.AllDirectories))
        {
            mapSelectDropdown.options.Add(new Dropdown.OptionData() { text = Path.GetFileName(mapFile) });
        }
        mapSelectDropdown.value = 1;
        mapSelectDropdown.value = 0;
    }

    public void loadExistingMap()
    {
        PlayerPrefs.SetInt("existingMap", 1);
        PlayerPrefs.SetString("mapName", mapSelectDropdown.options[mapSelectDropdown.value].text);
        SceneManager.LoadScene("MapEditor");
    }

    public void createNewMap()
    {
        PlayerPrefs.SetInt("existingMap", 0);
        PlayerPrefs.SetInt("width", Int32.Parse(widthField.text));
        PlayerPrefs.SetInt("height", Int32.Parse(heightField.text));
        SceneManager.LoadScene("MapEditor");
    }
}
