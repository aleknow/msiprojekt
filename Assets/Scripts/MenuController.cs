﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

    public Dropdown mapSelectDropdown;

    private string mapsDir;
    // Use this for initialization
    void Start () {
        
    }

    private void Awake(){
        mapsDir = Application.dataPath + "/Maps";
        loadMapsList();
    }

    // Update is called once per frame
    void Update () {
		
	}

    private void loadMapsList()
    {
        foreach (string mapFile in Directory.GetFiles(mapsDir, "*.xml", SearchOption.AllDirectories))
        {
            mapSelectDropdown.options.Add(new Dropdown.OptionData() { text = Path.GetFileName(mapFile) });
        }
        mapSelectDropdown.value = 1;
        mapSelectDropdown.value = 0;
    }

	public void startVisualisation()
	{
        string selectedMap = mapSelectDropdown.options[mapSelectDropdown.value].text;
        PlayerPrefs.SetString("selectedMap", selectedMap);
		SceneManager.LoadScene ("Game");
	}
	public void startMapEditor()
	{
		SceneManager.LoadScene ("EditorMenu");
	}
}
