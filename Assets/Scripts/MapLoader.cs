﻿using System;
using UnityEngine;
using System.Xml;
using System.IO;
using System.Collections.Generic;

public class MapLoader : MonoBehaviour {
    public static MapLoader instance = null;

    public const int GRASS_BLOCK_ID = 0;
    public const int WATER_BLOCK_ID = 1;
    public const int PAVEMENT_BLOCK_ID = 2;

    public Transform blankBlock;
	public Transform grassBlock;
	public Transform waterBlock;
	public Transform pavementBlock;
	public Transform dirtBlock;

	public Transform[,] loadMap(string mapPath){
		MapParser mapParser = new MapParser ();
		int[,] xmlMap = mapParser.loadMapFromXML (mapPath);

		Transform[,] finalMap = new Transform[xmlMap.GetLength(0), xmlMap.GetLength(1)];

		for (int x = 0; x < xmlMap.GetLength(0); x++) {
			for (int z = 0; z < xmlMap.GetLength(1); z++) {
				Instantiate (dirtBlock, new Vector3 (x, -1, z), Quaternion.identity);
				switch (xmlMap [x, z]) {
				case GRASS_BLOCK_ID:
					finalMap[x,z] = Instantiate (grassBlock, new Vector3 (x, 0, z), Quaternion.identity);	
					break;
				case WATER_BLOCK_ID:
					finalMap[x,z] = Instantiate (waterBlock, new Vector3 (x, 0, z), Quaternion.identity);
					break;
				case PAVEMENT_BLOCK_ID:
					finalMap[x,z] = Instantiate (pavementBlock, new Vector3 (x, 0, z), Quaternion.identity);
					break;
				}
			}
		}

		return finalMap;
	}

    public Transform[,] createEmptyMap(int width, int height)
    {
        Transform[,] finalMap = new Transform[width, height];

        for (int x = 0; x < width; x++)
        {
            for (int z = 0; z < height; z++)
            {
                Instantiate(dirtBlock, new Vector3(x, -1, z), Quaternion.identity);
                finalMap[x, z] = Instantiate(blankBlock, new Vector3(x, 0, z), Quaternion.identity);
            }
        }

        return finalMap;
    }

    /*public int[,] loadMapFromXML(string mapPath){
		int mapHeight;
		int mapWidth;
		
		int[,] map;

		using (XmlReader reader = XmlReader.Create (mapPath)) {
			reader.ReadToFollowing ("wysokosc");
			mapHeight = reader.ReadElementContentAsInt ();

			reader.ReadToFollowing ("szerokosc");
			mapWidth = reader.ReadElementContentAsInt ();

			map = new int[mapWidth, mapHeight];

			while (reader.Read ()) {
				switch (reader.Name) {
				case "pole":
					reader.ReadToFollowing ("x");
					int x = reader.ReadElementContentAsInt ();
					reader.ReadToFollowing ("y");
					int y = reader.ReadElementContentAsInt ();
					reader.ReadToFollowing ("typ");
					int type = reader.ReadElementContentAsInt ();
					map [x, y] = type;
					break;
				}
			}
		}

		return map;
	}*/

    public void saveMap(Transform[,] map, string mapName)
    {
        int[,] idMap = createIdMap(map);
        MapParser mapParser = new MapParser();
        mapParser.saveMapToXML(idMap, mapName);
    }

    private int[,] createIdMap(Transform[,] map)
    {
        int[,] idMap = new int[map.GetLength(0), map.GetLength(1)];
        for (int x = 0; x < map.GetLength(0); x++)
        {
            for (int z = 0; z < map.GetLength(1); z++)
            {
                if (map[x, z].name == grassBlock.name + "(Clone)")
                {
                    idMap[x, z] = GRASS_BLOCK_ID;
                }
                else if (map[x, z].name == waterBlock.name + "(Clone)")
                {
                    idMap[x, z] = WATER_BLOCK_ID;
                }
                else if (map[x, z].name == pavementBlock.name + "(Clone)")
                {
                    idMap[x, z] = PAVEMENT_BLOCK_ID;
                }
            }
        }
        return idMap;
    }
}

