var start = false, meta = false;
var corvo = false, attano = true;
var m1 = null, m2 = null;
var m1pos, m2pos;
var sourceIcon = 'source.png', destinationIcon = 'destination.png';
var strokeColor = '#131540';
var strokeOpacity = 0.6;
var strokeWeight = 6;
var markers = [];
var map
var startLatLng = 0, metaLatLng = 0;
var currentColor;

$(document).ready(function () {
    map = new google.maps.Map
    (
      document.getElementById("map"), {
          center: new google.maps.LatLng(54.347156, 18.639034),
          zoom: 12,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
      });
    google.maps.event.addListener(map, 'click', function (event) {
        placeMarker(event.latLng);
    });
    

    $('#loc_text').keypress(function (e) {
        if (e.which == 13) {
            var geocoder = new google.maps.Geocoder();
            
            var address = $('#loc_text').val();
            geocodeAddress(geocoder, map, address);
        }
    });

    $('#loc_button').click(function () {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                map.setCenter(pos);
            });
        }
    });

    $('#trace_route').click(function () {
        var directionsService = new google.maps.DirectionsService();
        var directionsDisplay = new google.maps.DirectionsRenderer();
        calculateAndDisplayRoute(startLatLng, metaLatLng, directionsService, directionsDisplay);
    });
    $('#new_route').click(function () {
        // Remove previous route
        clearMarkers();
        markers = [];
        start = false;
        meta = false;
    });
});

function clearMarkers() {
    setMapOnAll(null);
}
function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

function renderDirections(result, i) {
    var directionsRenderer1 = new google.maps.DirectionsRenderer({
        directions: result,
        routeIndex: i,
        map: map,
        polylineOptions: {
            strokeColor: currentColor
        }
    });
}

function calculateAndDisplayRoute(origin, destination, directionsService, directionsDisplay) {

    directionsService.route({
        origin: origin,
        destination: destination,
        travelMode: google.maps.TravelMode.WALKING,
        provideRouteAlternatives: true
    }, function (response, status) {
        if (status === google.maps.DirectionsStatus.OK) {
            window.alert(response.routes.length + " ROUTES FOUND");
            clearMarkers();
            markers = [];
            for (i = 0; i < response.routes.length; i++) {
                getColor(i);
                renderDirections(response, i);
                var legs = response.routes[i].legs;
                calcDistanceAndDuration(legs, i);
            }

        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
}

function getColor(i) {
    if (i == 0) {
        currentColor = "red";
    }
    if (i == 1) {
        currentColor = "green";
    }
    if (i == 2) {
        currentColor = "blue";
    }
    if (i == 3) {
        currentColor = "pink";
    }
    if (i == 4) {
        currentColor = "yellow";
    }
}

function calcDistanceAndDuration(legs, i) {
    var totalDistance = 0, totalDuration = 0;
    for (var i = 0; i < legs.length; ++i) {
        totalDistance += legs[i].distance.value;
        totalDuration += legs[i].duration.value;
    }
    window.alert("TOTAL DISTANCE OF ROUTE " + i + " IS " + totalDistance/1000);
    window.alert("TOTAL DURATION OF ROUTE " + i + " IS " + totalDuration/60);	// to cos srednio dziala
}

function geocodeAddress(geocoder, resultsMap, address) {
    geocoder.geocode({ 'address': address }, function (results, status) {
        if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            placeMarker(results[0].geometry.location);
           /* var marker = new google.maps.Marker({
                map: resultsMap,
                position: results[0].geometry.location
            });*/
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}
function placeMarker(location) {
    if (start == false) {
        var marker = new google.maps.Marker({
            position: location,
            map: map,
            title: 'START!',
            label: 'A'
        });
        startLatLng = location;
        markers.push(marker);

        start = true;
    }
    else if (meta == false) {
        var marker = new google.maps.Marker({
            position: location,
            map: map,
            title: 'META!',
            label: 'B'
        });
        metaLatLng = location;
        markers.push(marker);
        meta = true;
    }
}